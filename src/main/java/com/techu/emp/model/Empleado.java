package com.techu.emp.model;

import lombok.*;

import java.util.ArrayList;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Getter
@Setter
public class Empleado {

    private Integer id;
    private String nombre;
    private String Apellidos;
    private String email;
    private ArrayList<Formacion> formaciones;

}
