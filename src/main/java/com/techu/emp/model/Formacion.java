package com.techu.emp.model;

import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Getter
@Setter
public class Formacion {
    private String fecha;
    private String titulo;
}
