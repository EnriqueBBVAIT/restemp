package com.techu.emp.model;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Setter
public class Empleados {

    private List<Empleado> listaEmpleados;

    public List<Empleado> getListaEmpleados() {
        if (listaEmpleados == null) {
            listaEmpleados = new ArrayList<Empleado>();

        }
        return listaEmpleados;
    }
}
