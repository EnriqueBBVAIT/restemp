package com.techu.emp.repositories;

import com.techu.emp.model.Empleado;
import com.techu.emp.model.Empleados;
import com.techu.emp.model.Formacion;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Repository
public class EmpleadosDAO {

    private static Empleados list = new Empleados();

    static {
        Formacion form1 = new Formacion("2021/01/01", "Curso Back");
        Formacion form2 = new Formacion("2021/01/01", "Curso Front");
        Formacion form3 = new Formacion("2021/01/01", "Curso SQL");
        ArrayList<Formacion> una = new ArrayList<Formacion>();
        una.add(form1);
        ArrayList<Formacion> dos = new ArrayList<Formacion>();
        dos.add(form1);
        dos.add(form2);
        ArrayList<Formacion> todas = new ArrayList<Formacion>();
        todas.add(form3);
        todas.addAll(dos);

        list.getListaEmpleados().add(new Empleado(1, "Enrique", "Sánchez Barquilla", "yo@gmail.com", una));
        list.getListaEmpleados().add(new Empleado(2, "PJ", "IDK", "pj@gmail.com", dos));
        list.getListaEmpleados().add(new Empleado(3, "Dani", "IDK", "dani@gmail.com", todas));

    }

    public Empleados getAllEmpleados() {
        return list;
    }

    public Empleado getEmpleado(int id) {
        for (Empleado emp : list.getListaEmpleados()) {
            if (emp.getId() == id) {
                return emp;
            }
        }
        return null;
    }

    public void addEmpleado(Empleado emp) {
        list.getListaEmpleados().add(emp);
    }

    public Empleado updateEmpleado(Empleado emp) {

        Empleado currentEmp = getEmpleado(emp.getId());

        if (currentEmp != null) {
            currentEmp.setNombre(emp.getNombre());
            currentEmp.setApellidos(emp.getApellidos());
            currentEmp.setEmail(emp.getEmail());

        }

        return currentEmp;
    }

    public Empleado updateEmpleado(int id, Empleado emp) {

        Empleado currentEmp = getEmpleado(id);

        if (currentEmp != null) {
            currentEmp.setNombre(emp.getNombre());
            currentEmp.setApellidos(emp.getApellidos());
            currentEmp.setEmail(emp.getEmail());

        }

        return currentEmp;
    }

    public void deleteEmpleado(int id) {
        Iterator<Empleado> iterator = list.getListaEmpleados().iterator();
        while (iterator.hasNext()) {
            Empleado empleado = (Empleado) iterator.next();
            if (empleado.getId() == id) {
                iterator.remove();
                break;
            }

        }
    }

    public Empleado softUpdEmpleado(int id, Map<String, Object> updates) {
        Empleado current = getEmpleado(id);
        if (current != null) {
            for (Map.Entry<String, Object> update : updates.entrySet()) {
                switch (update.getKey()) {
                    case "nombre":
                        current.setNombre(update.getValue().toString());
                        break;
                    case "apellidos":
                        current.setApellidos(update.getValue().toString());
                        break;
                    case "email":
                        current.setEmail(update.getValue().toString());
                        break;
                }
            }
        }
        return current;
    }

    public List<Formacion> getFormacionEmpleado(int id) {
        for (Empleado empleado : list.getListaEmpleados()) {
            if (empleado.getId() == id) {
                return empleado.getFormaciones();
            }
        }
        return null;
    }

    public Empleado addFormacionEmpleado(int id, Formacion formacion) {

        Empleado current = getEmpleado(id);
        if (current != null) {
            current.getFormaciones().add(formacion);
        }
        return current;
    }
}
