package com.techu.emp.controllers;

import com.techu.emp.model.Empleado;
import com.techu.emp.model.Empleados;
import com.techu.emp.model.Formacion;
import com.techu.emp.repositories.EmpleadosDAO;
import com.techu.emp.utils.Config;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path = "/empleados")
public class EmpleadosControllerImpl {

    @Autowired
    private EmpleadosDAO empleadosDAO;


    @GetMapping(path = "/", produces = "application/joson")
    public Empleados getEmpleados() {
        return empleadosDAO.getAllEmpleados();
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<Empleado> getEmpleado(@PathVariable int id) {
        Empleado empleado = empleadosDAO.getEmpleado(id);
        if (empleado != null) return new ResponseEntity<>(empleado, HttpStatus.OK);
        else return ResponseEntity.notFound().build();
    }

    @PostMapping(path = "/", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> addEmpleado(@RequestBody Empleado emp) {
        Integer id = empleadosDAO.getAllEmpleados().getListaEmpleados().size() + 1;
        emp.setId(id);
        empleadosDAO.addEmpleado(emp);
        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(emp.getId())
                .toUri();
        return ResponseEntity.created(uri).build();
    }

    @PutMapping(path = "/", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> updateEmpleado(@RequestBody Empleado emp) {
        Empleado upEmp = empleadosDAO.updateEmpleado(emp);
        if (upEmp == null) {
            return ResponseEntity.notFound().build();

        } else {
            return ResponseEntity.ok().build();
        }
    }

    @PutMapping(path = "/{id}", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> updateEmpleado(@PathVariable int id, @RequestBody Empleado emp) {

        Empleado upEmp = empleadosDAO.updateEmpleado(id, emp);
        if (upEmp == null) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok().build();
        }
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Object> deleteEmpleado(@PathVariable int id) {
        empleadosDAO.deleteEmpleado(id);
        return ResponseEntity.ok().build();

    }

    @PatchMapping(path = "/{id}", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> patchEmpleado(@PathVariable int id, @RequestBody Map<String, Object> updates) {
        Empleado empleado = empleadosDAO.softUpdEmpleado(id, updates);

        if (empleado == null) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok().build();
        }
    }

    @GetMapping(path = "/{id}/formaciones", produces = "application/json")
    public ResponseEntity<List<Formacion>> getFormacionesEmpleado(@PathVariable int id) {
        List<Formacion> formacionList = empleadosDAO.getFormacionEmpleado(id);

        if (formacionList == null) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok().body(formacionList);
        }
    }

    @PostMapping(path = "/{id}/formaciones", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> addFormacionEmpleado(@PathVariable int id, @RequestBody Formacion formacion) {
        Empleado emp = empleadosDAO.addFormacionEmpleado(id, formacion);

        if (formacion == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }

    @Value("${app.titulo")
    private String titulo;

    @GetMapping("/titulo")
    public String getTitulo() {
        return titulo;
    }

    @Autowired
    private Environment environment;

    @GetMapping("/prop")
    public String getPropiedad(@RequestParam("key") String key) {
        String valor = "Sin Valor";
        String keyValor = environment.getProperty(key);

        if (keyValor != null && !keyValor.isEmpty()) {
            valor = keyValor;
        }
        return valor;
    }

    @Autowired
    private Config config;

    @GetMapping("/autor")
    public String getAutor() {
        return config.getAutor();
    }
}
